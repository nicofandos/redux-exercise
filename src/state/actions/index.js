export const pressNumber = (numberPressed) => ({
  type: 'NUMBER_PRESSED',
  numberPressed
});

export const pressDot = () => ({
  type: 'DOT_PRESSED'
});

export const pressUnaryOperation = (operation) => ({
  type: 'UNARY_OPERATION_PRESSED',
  operation
});

export const pressBinaryOperation = (operation) => ({
  type: 'BINARY_OPERATION_PRESSED',
  operation
});

export const pressSumation = () => ({
  type: 'SUMMATION_PRESSED'
})

export const pressUndo = () => ({
  type: 'UNDO_PRESSED'
});

export const pressIntro = () => ({
  type: 'INTRO_PRESSED'
});
