export const undoPressed = (state) => {
  if (state.history.length === 0)
    return state;
  return {
    ...state.history.pop()
  };
};
