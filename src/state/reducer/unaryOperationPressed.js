import { DEFAULT_STATE } from './constants'

export const unaryOperationPressed = (state, action) => {
  if (state.currentStack.length === 0 && state.currentNumber === 0)
    return state;
  let currentStack = [...state.currentStack];
  if (state.currentNumber !== 0)
    currentStack.push(state.currentNumber);
  let op1 = currentStack.pop();
  currentStack.push(action.operation(op1));
  return {
    ...state,
    currentStack: [...currentStack],
    currentNumber: DEFAULT_STATE.currentNumber,
    fractionalPartLength: DEFAULT_STATE.fractionalPartLength,
    history: [...state.history, state]
  };
};
