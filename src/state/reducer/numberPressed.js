export const numberPressed = (state, action) => {
  if (state.fractionalPartLength > 0)
    return {
      ...state,
      currentNumber: state.currentNumber + (action.numberPressed / ( Math.pow(10, state.fractionalPartLength) )),
      fractionalPartLength: state.fractionalPartLength + 1,
      history: [...state.history, state]
    };
  return {
    ...state,
    currentNumber: state.currentNumber * 10 + action.numberPressed,
    history: [...state.history, state]
  }
};
