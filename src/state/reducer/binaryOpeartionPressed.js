import { DEFAULT_STATE } from './constants'

export const binaryOpeartionPressed = (state, action) => {
  if (state.currentStack.length === 0 || (state.currentStack.length === 1 && state.currentNumber === 0))
    return state;
  let currentStack = [...state.currentStack];
  if (state.currentNumber !== 0)
    currentStack.push(state.currentNumber);
  let op1 = currentStack.pop();
  let op2 = currentStack.pop();
  currentStack.push(action.operation(op1, op2));
  return {
    ...state,
    currentStack: [...currentStack],
    currentNumber: DEFAULT_STATE.currentNumber,
    fractionalPartLength: DEFAULT_STATE.fractionalPartLength,
    history: [...state.history, state]
  };
};
