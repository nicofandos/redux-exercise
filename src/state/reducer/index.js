import { numberPressed } from './numberPressed';
import { dotPressed } from './dotPressed';
import { unaryOperationPressed } from './unaryOperationPressed';
import { binaryOpeartionPressed } from './binaryOpeartionPressed';
import { summationPressed } from './summationPressed';
import { undoPressed } from './undoPressed';
import { introPressed } from './introPressed';
import { DEFAULT_STATE } from './constants'

export const rootReducer = (state, action) => {
  if (state === undefined) {
    return {...DEFAULT_STATE};
  }

  switch (action.type) {
    case 'NUMBER_PRESSED':
      return numberPressed(state, action);
    case 'UNARY_OPERATION_PRESSED':
      return unaryOperationPressed(state, action);
    case 'BINARY_OPERATION_PRESSED':
      return binaryOpeartionPressed(state, action);
    case 'SUMMATION_PRESSED':
      return summationPressed(state);
    case 'DOT_PRESSED':
      return dotPressed(state);
    case 'UNDO_PRESSED':
      return undoPressed(state);
    case 'INTRO_PRESSED':
      return introPressed(state);
    default:
      return state;
  }
};
