export const DEFAULT_STATE = {
  currentNumber: 0,
  currentStack: [],
  fractionalPartLength: 0,
  history: []
}
