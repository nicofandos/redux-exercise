export const dotPressed = (state) => {
  if (state.fractionalPartLength > 0)
    return state;
  return {
    ...state,
    fractionalPartLength: 1,
    history: [...state.history, state]
  };
};
