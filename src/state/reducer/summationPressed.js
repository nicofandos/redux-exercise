import { DEFAULT_STATE } from './constants'

export const summationPressed = (state) => {
  if (state.currentStack.length === 0 && state.currentNumber === 0)
    return state;
  let currentStack = [...state.currentStack];
  if (state.currentNumber !== 0)
    currentStack.push(state.currentNumber);
  return {
    ...state,
    currentStack: [currentStack.reduce((acum, current) => {return acum + current}, 0)],
    currentNumber: DEFAULT_STATE.currentNumber,
    fractionalPartLength: DEFAULT_STATE.fractionalPartLength,
    history: [...state.history, state]
  };
};
