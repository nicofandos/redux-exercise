import { DEFAULT_STATE } from './constants'

export const introPressed = (state) => {
  return {
    ...state,
    currentStack: [...state.currentStack, state.currentNumber],
    currentNumber: DEFAULT_STATE.currentNumber,
    fractionalPartLength: DEFAULT_STATE.fractionalPartLength,
    history: [...state.history, state]
  };
};
