import { useDispatch, useSelector } from 'react-redux';

import { pressNumber, pressDot, pressUnaryOperation,
         pressBinaryOperation, pressSumation, 
         pressUndo, pressIntro } from 'state/actions';
import { selectCurrentNumber, selectCurrentStack } from 'state/selectors';

import styles from './Calculator.module.css';

const renderStackItem = (value, index) => {
  return <div key={index}>{value}</div>;
};

export const Calculator = () => {
  const currentNumber = useSelector(selectCurrentNumber);
  const stack = useSelector(selectCurrentStack);

  const dispatch = useDispatch();
  const onClickNumber = (number) => {
    const action = pressNumber(number);
    dispatch(action);
  };
  const onClickDot = () => {
    const action = pressDot();
    dispatch(action);
  }
  const onClickUnaryOperation = (operation) => {
    const action = pressUnaryOperation(operation);
    dispatch(action);
  }
  const onClickBinaryOperation = (operation) => {
    const action = pressBinaryOperation(operation);
    dispatch(action);
  }
  const onClickSummation = () => {
    const action = pressSumation();
    dispatch(action);
  };
  const onClickIntro = () => {
    const action = pressIntro();
    dispatch(action);
  }
  const onClickUndo = () => {
    const action = pressUndo();
    dispatch(action);
  }

  return (
    <div className={styles.main}>
      <div className={styles.display}>{currentNumber}</div>
      <div className={styles.numberKeyContainer}>
        {[...Array(9).keys()].map((i) => (
          <button key={i} onClick={() => onClickNumber(i + 1)}>
            {i + 1}
          </button>
        ))}
        <button className={styles.zeroNumber} onClick={() => onClickNumber(0)}>
          0
        </button>
        <button onClick={() => onClickDot()}>.</button>
      </div>
      <div className={styles.opKeyContainer}>
        <button onClick={() => onClickBinaryOperation((op1, op2) => { return op1 + op2 })}>+</button>
        <button onClick={() => onClickBinaryOperation((op1, op2) => { return op1 - op2 })}>-</button>
        <button onClick={() => onClickBinaryOperation((op1, op2) => { return op1 * op2 })}>x</button>
        <button onClick={() => onClickBinaryOperation((op1, op2) => { return op1 / op2 })}>/</button>
        <button onClick={() => onClickUnaryOperation((op1) => { return Math.sqrt(op1) })}>√</button>
        <button onClick={() => onClickSummation()}>Σ</button>
        <button onClick={() => onClickUndo()}>Undo</button>
        <button onClick={() => onClickIntro()}>Intro</button>
      </div>
      <div className={styles.stack}>{stack.map(renderStackItem)}</div>
    </div>
  );
};
